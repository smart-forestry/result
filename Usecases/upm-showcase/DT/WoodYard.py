import json
from dt_templates.json_models import wood_yard
from ml.fml40.features.functionalities.accepts_log_loading_unit_in_wood_yard import AcceptsLogLoadingUnitInWoodYard
from ml import APP_LOGGER, setup_logger, build, S3IConnector, Thing
import requests
import asyncio
import ast


class MyS3IConnector(S3IConnector):
    def __init__(self, loop, entry):
        with open("config/cred.json", "r") as cred_str:
            cred_json = json.load(cred_str)
            oauth2_id = cred_json["wood_yard"]["id"]
            oauth2_secret = cred_json["wood_yard"]["secret"]

        super().__init__(
            oauth2_id=oauth2_id,
            oauth2_secret=oauth2_secret,
            is_repository=False,
            is_broker_rest=False,
            is_broker=True,
            dt_entry_ins=entry,
            loop=loop
        )

    def send_event_new_volume_in_wood_yard(self, box_id, species_group, volume: dict):
        content = {
            "storageBoxId": box_id,
            "speciesGroup": species_group,
            "volume": volume
        }
        APP_LOGGER.info(f"Send new volume in wood yard to forest owner: {json.dumps(content, indent=2)}")
        self.add_broker_event_message_to_send(
            "{}.{}".format(self.dt_entry_ins.identifier, "newVolumeInWoodYard"),
            content
        )

    def send_event_new_forecast_in_wood_yard(self, box_id, species_group, volume:dict, forecast_volume: dict):
        # TODO: how will this method be called?
        content = {
            "storageBoxId": box_id,
            "speciesGroup": species_group,
            "volume": volume,
            "forecastVolume": forecast_volume
        }
        self.add_broker_event_message_to_send(
            "{}.{}".format(self.dt_entry_ins.identifier, "newForecastInWoodYard"),
            content
        )

class AcceptsLogLoadingUnitInWoodYardImpl(AcceptsLogLoadingUnitInWoodYard):
    def __init__(self, wood_yard, name="", identifier=""):
        super(AcceptsLogLoadingUnitInWoodYardImpl, self).__init__(
            name=name,
            identifier=identifier
        )
        self.wood_yard = wood_yard

    async def accept(self, receiptNumber, logLoadingUnit):
        APP_LOGGER.info("Accept log loading unit from Mill Gate")
        result = self.wood_yard.stores_in_log_storage_box(logLoadingUnit)
        self.wood_yard.delete_log_loading_unit(logLoadingUnit["thingId"])
        return {
            "receiptNumber": receiptNumber,
            "storageBoxIds": result
        }


class WoodYard(Thing):
    def __init__(self):
        with open("config/cred.json", "r") as cred_str:
            cred_json = json.load(cred_str)
            identifier = cred_json["wood_yard"]["id"]
            self.__wood_yard_url = cred_json["wood_yard"]["host"]
            wood_yard["thingId"] = identifier
            wood_yard["policyId"] = identifier
            wood_yard["attributes"]["name"] = "BaySF Virtual WoodYard"
            self.__log_storage_boxes = cred_json["baysf_storage_boxes"]
            self.__log_storage_box_ids = []
            for feature in wood_yard["attributes"]["features"]:
                if feature["class"] == "ml40::Composite":
                    feature["targets"] = cred_json["baysf_storage_boxes"]
            wood_yard["attributes"]["features"].append(
                {
                    "class": "ml40::EventList",
                    "subFeatures": [
                        {
                            "class": "ml40::Event",
                            "topic": f"{identifier}.newVolumeInWoodYard".format(identifier),
                            "description": "",
                            "frequency": 1,
                            "schema": {},
                            "exampleContent":  {
                                "storageBoxId": "123",
                                "speciesGroup": "species_group",
                                "volume": {
                                    "currentVolume": 0.123,
                                    "practicalMaxFillingVolume": 50.123,
                                    "practicalMinFillingVolume": 20.312,
                                    "theoreticalMaxFillingVolume": 100.31,
                                    "theoreticalMinFillingVolume": 10.21
                                }
                            }
                        },
                        {
                            "class": "ml40::Event",
                            "topic": f"{identifier}.newForecastVolumeInWoodYard",
                            "description": "",
                            "frequency": 1,
                            "schema": {},
                            "exampleContent": {
                                "storageBoxId": "123",
                                "speciesGroup": "kie",
                                "volume": {
                                    "currentVolume": 0.123,
                                    "practicalMaxFillingVolume": 50.123,
                                    "practicalMinFillingVolume": 20.312,
                                    "theoreticalMaxFillingVolume": 100.31,
                                    "theoreticalMinFillingVolume": 10.21
                                },
                                "forecastVolume": {
                                    "volume": 20.21,
                                    "before": "2025-01-02" 
                                }
                            }
                        }
                    ]
                }
            )
        setup_logger("WoodYard")
        loop = asyncio.get_event_loop()
        entry = build(wood_yard)
        connector = MyS3IConnector(loop=loop, entry=entry)
        super().__init__(
            entry=entry,
            loop=loop,
            connector=connector
        )

    @property
    def wood_yard_url(self):
        return self.__wood_yard_url

    def init_log_storage_box(self):
        for box in self.__log_storage_boxes:
            box_entry = build({"attributes": box})
            storage_box_id = box_entry.identifier
            product_name = box_entry.features["fml40::RoundWoodProduct"].productName
            species_group = box_entry.features["fml40::SpeciesGroup"].name
            theo_max_filling_quantity = box_entry.features["fml40::VirtualWoodYardLoadingProperty"].theoreticalMaxFillingQuantity
            theo_min_filling_quantity = box_entry.features["fml40::VirtualWoodYardLoadingProperty"].theoreticalMinFillingQuantity
            pra_max_filling_quantity = box_entry.features["fml40::VirtualWoodYardLoadingProperty"].practicalMaxFillingQuantity
            pra_min_filling_quantity = box_entry.features["fml40::VirtualWoodYardLoadingProperty"].practicalMinFillingQuantity
            current_filling_quantity = box_entry.features["fml40::VirtualWoodYardLoadingProperty"].currentFillingQuantity
            location = {
                "longitude": box_entry.features["ml40::Location"].longitude,
                "latitude": box_entry.features["ml40::Location"].latitude
            }
            resp = requests.post(
                url=self.__wood_yard_url + "/storage_box/",
                json={
                    "storageBoxId": storage_box_id,
                    "speciesGroup": species_group,
                    "theoreticalMaxFillingQuantity": theo_max_filling_quantity,
                    "theoreticalMinFillingQuantity": theo_min_filling_quantity,
                    "roundWoodProductName": product_name,
                    "practicalMaxFillingQuantity": pra_max_filling_quantity,
                    "practicalMinFillingQuantity": pra_min_filling_quantity,
                    "currentFillingQuantity": current_filling_quantity,
                    "location": location
                }
            )
            if resp.status_code == 201:
                self.__log_storage_box_ids.append(resp.text)
                APP_LOGGER.info("Id of the storage box: {}".format(resp.text))
            else:
                APP_LOGGER.critical(resp.text)

    def stores_in_log_storage_box(self, log_loading_unit):
        """
        :param log_loading_unit:
        :type log_loading_unit: dict
        :return:
        """
        # TODO: Check the assortment of the log loading unit and then insert to the respective log storage box
        log_loading_unit_entry = build(log_loading_unit)
        log_loading_unit_entry.refresh_dt_json()
        result = []
        wood_pile_list_entry = log_loading_unit_entry.features["fml40::WoodPileList"]
        for key in wood_pile_list_entry.targets.keys():
            species_group = wood_pile_list_entry.targets[key].features["fml40::SpeciesGroup"].name
            wood_volume = wood_pile_list_entry.targets[key].features["fml40::ProductVolume"].value
            storage_boxes_resp = requests.get(
                url=self.__wood_yard_url + "storage_box/all"
            )
            if storage_boxes_resp.status_code == 200 and storage_boxes_resp.text is not None:
                for box in ast.literal_eval(storage_boxes_resp.text):
                    if box[1] == species_group:
                        # Get the current box filling volume
                        box_cur_volume = requests.get(
                            url=self.__wood_yard_url + "storage_box_access/current_filling_quantity/{}".format(box[0])).text
                        # Find the storage box in the entry
                        for _key in self.entry.features["ml40::Composite"].targets.keys():
                            if self.entry.features["ml40::Composite"].targets[_key].identifier == box[0]:
                                self.entry.features["ml40::Composite"].targets[_key].features[
                                    "fml40::VirtualWoodYardLoadingProperty"].currentFillingQuantity = float(box_cur_volume) + float(wood_volume)
                                APP_LOGGER.info("Changing current filling quantity from {} to {} to the storage box {} with species group {}".format(
                                    box_cur_volume, float(box_cur_volume) + float(wood_volume), box[0], box[1]
                                ))
                                requests.post(
                                    url=self.__wood_yard_url + "storage_box_access/current_filling_quantity/{}".format(box[0]),
                                    json={
                                        "value": float(box_cur_volume) + float(wood_volume)
                                    }
                                )
                                requests.post(
                                    url=self.__wood_yard_url + "storage_box_access/log_loading_units/{}".format(box[0]),
                                    json={
                                        "value": json.dumps(log_loading_unit_entry.dt_json, indent=2)
                                    }
                                )
                                volume_property = self.entry.features["ml40::Composite"].targets[_key].features["fml40::VirtualWoodYardLoadingProperty"]

                                volume = {
                                    "currentVolume": volume_property.currentFillingQuantity,
                                    "practicalMaxFillingVolume": volume_property.practicalMaxFillingQuantity,
                                    "practicalMinFillingVolume": volume_property.practicalMinFillingQuantity,
                                    "theoreticalMaxFillingVolume": volume_property.theoreticalMaxFillingQuantity,
                                    "theoreticalMinFillingVolume": volume_property.theoreticalMinFillingQuantity
                                }
                                self.connector.send_event_new_volume_in_wood_yard(
                                    box_id=box[0],
                                    species_group=box[1],
                                    volume=volume
                                )
                                if box[0] in result:
                                    continue
                                result.append(box[0])
        return result

    @staticmethod
    def delete_log_loading_unit(identifier):
        APP_LOGGER.info("Delete log loading unit {}".format(identifier))

    def run(self):
        self.add_ml40_implementation(AcceptsLogLoadingUnitInWoodYardImpl,
                                     "fml40::AcceptsLogLoadingUnitInWoodYard",
                                     self)
        self.add_on_thing_start_ok_callback(self.init_log_storage_box, True, False)
        self.run_forever()
