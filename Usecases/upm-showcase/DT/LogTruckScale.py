import json
from dt_templates.json_models import log_truck_scale
from ml import Thing, S3IConnector, build, setup_logger, APP_LOGGER
from ml.fml40.features.functionalities.accepts_log_truck_weight_measurement import AcceptsLogTruckWeightMeasurement
import aioconsole
import asyncio


class AcceptsLogTruckWeightMeasurementImpl(AcceptsLogTruckWeightMeasurement):
    def __init__(self, labor, name="", identifier=""):
        super(AcceptsLogTruckWeightMeasurementImpl, self).__init__(
            name=name,
            identifier=identifier
        )
        self.labor = labor

    async def acceptFullWeightMeasurement(self, receiptNumber):
        while True:
            weight = await aioconsole.ainput("Please enter the measured full weight of the log truck: ")
            try:
                weight_float = float(weight)
                APP_LOGGER.info("Send the full weight {} back to mill gate".format(weight))
                return {"weight": float(weight_float), "receiptNumber": receiptNumber}
            except ValueError:
                APP_LOGGER.critical("Please enter a numerical value")
                continue

    async def acceptEmptyWeightMeasurement(self, receiptNumber):
        while True:
            weight = await aioconsole.ainput("Please enter the measured empty weight of the log truck: ")
            try:
                weight_float = float(weight)
                APP_LOGGER.info("Send the empty weight {} back to mill gate".format(weight))
                return {"weight": float(weight_float), "receiptNumber": receiptNumber}
            except ValueError:
                APP_LOGGER.critical("Please enter a numerical value")
                continue


class LogTruckScale(Thing):
    def __init__(self):
        with open("config/cred.json", "r") as cred_str:
            cred_json = json.load(cred_str)
            oauth2_id = cred_json["log_truck_scale"]["id"]
            oauth2_secret = cred_json["log_truck_scale"]["secret"]
        log_truck_scale["thingId"] = oauth2_id
        log_truck_scale["policyId"] = oauth2_id
        log_truck_scale["attributes"]["name"] = "UPM Scale"
        setup_logger("UPM Scale")
        loop = asyncio.get_event_loop()
        entry = build(log_truck_scale)
        connector = S3IConnector(
            oauth2_id=oauth2_id,
            oauth2_secret=oauth2_secret,
            is_repository=False,
            is_broker_rest=False,
            is_broker=True,
            dt_entry_ins=entry,
            loop=loop
        )
        super(LogTruckScale, self).__init__(
            loop=loop,
            entry=entry,
            connector=connector
        )

    def update_current_weight(self):
        pass

    def run(self):
        self.add_ml40_implementation(AcceptsLogTruckWeightMeasurementImpl,
                                     "fml40::AcceptsLogTruckWeightMeasurement", self)
        self.run_forever()
