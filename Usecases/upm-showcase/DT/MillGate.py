import uuid
import json
from s3i.broker_message import ServiceRequest, UserMessage
from dt_templates.json_models import mill_gate
from ml import APP_LOGGER, setup_logger, build, S3IConnector, Thing, Entry
from ml.fml40.features.functionalities.accepts_log_loading_unit import AcceptsLogLoadingUnit, Result
import requests
import aioconsole
import asyncio
import ast
import time


class AcceptsLogLoadingUnitImpl(AcceptsLogLoadingUnit):
    def __init__(self, mill_gate, name="", identifier=""):
        super(AcceptsLogLoadingUnitImpl, self).__init__(
            name=name,
            identifier=identifier
        )
        self.mill_gate = mill_gate

    async def accept(self, logLoadingUnit: dict) -> Result:
        llu_entry = build(logLoadingUnit)
        APP_LOGGER.info(f"Receiving a log loading unit (id: {llu_entry.identifier} with EUDR (ref.Nr.: {llu_entry.features['fml40::EUDR'].referenceNumber}, val.Nr.:{llu_entry.features['fml40::EUDR'].validationNumber})")
        self.mill_gate.log_loading_unit_entry = llu_entry
        self.mill_gate.log_loading_unit_entry.refresh_dt_json()
        reference_number = ""
        contract_number = ""
        visual_check = await aioconsole.ainput(
            "Please enter [1] for successful visual check, [2] for failed visual check: ")
        if visual_check == "2":
            APP_LOGGER.critical("Visual checking failed, no delivery will be created")
            return {
                "ok": False,
                "identifier": logLoadingUnit.get("thingId")
            }
        for feature in logLoadingUnit["attributes"].get("features"):
            if feature["class"] == "ml40::ContractNumber":
                contract_number = feature["value"]
            elif feature["class"] == "fml40::TransportOrderId":
                reference_number = feature["value"]
        response = requests.post(
            url=self.mill_gate.vato_url + "delivery",
            json={
                "referenceNr": reference_number,
                "contractNr": contract_number,
                "logLoadingUnit": logLoadingUnit
            }
        )
        if response.status_code == 201:
            receipt_number = response.text
            APP_LOGGER.info("Delivery created in the Dummy VATO, receipt number: {}".format(receipt_number))
            self.mill_gate.connector.send_wood_moisture_measurement_job(receipt_number)
            APP_LOGGER.info("Log loading unit (ID: {}) has been accepted, "
                            "receipt number {}".format(logLoadingUnit.get("thingId"), receipt_number))
            return {
                "ok": True,
                "identifier": logLoadingUnit.get("thingId")
            }
        else:
            unfinished_deliveries_resp = requests.get(url=self.mill_gate.vato_url + "delivery/unfinished")
            for delivery in unfinished_deliveries_resp.json():
                self.mill_gate.connector.send_wood_moisture_measurement_job(delivery)
                APP_LOGGER.critical("Delivery not created")
                APP_LOGGER.critical("Log loading unit (ID: {}) has been rejected, because delivery already exists: "
                                    "receipt number {}".format(logLoadingUnit.get("thingId"), delivery))
            return {
                "ok": False,
                "identifier": logLoadingUnit.get("thingId")
            }


class MyS3IConnector(S3IConnector):
    def __init__(self, loop, entry, vato_url, oauth2_id, oauth2_secret):
        with open("config/cred.json", "r") as cred_str:
            self.__cred_json = json.load(cred_str)
        self.__vato_url = vato_url
        super().__init__(
            oauth2_id=oauth2_id,
            oauth2_secret=oauth2_secret,
            is_repository=True,
            is_broker_rest=False,
            is_broker=True,
            dt_entry_ins=entry,
            loop=loop
        )

    def send_unload_request(self, llu_id, receipt_number):
        serv_req = ServiceRequest()
        serv_req.fillServiceRequest(
            sender=self.dt_entry_ins.identifier,
            receivers=[self.__cred_json["truck_app"]["id"]],
            message_id="s3i:{}".format(str(uuid.uuid4())),
            reply_to_endpoint=self.find_msg_broker_endpoint(self.dt_entry_ins.identifier),
            service_type="fml40::UnloadsLogLoadingUnit/unload",
            parameters={
                "identifier": llu_id
            }
        )
        APP_LOGGER.info("Send message to request unload log loading unit")

        self.add_broker_message_to_send(
            [f"s3ib://{self.__cred_json['truck_app']['id']}"],
            serv_req.base_msg
        )

    def send_log_truck_full_measurement_job(self, receipt_number):
        serv_req = ServiceRequest()
        serv_req.fillServiceRequest(
            sender=self.dt_entry_ins.identifier,
            receivers=[self.__cred_json["log_truck_scale"]["id"]],
            message_id="s3i:{}".format(str(uuid.uuid4())),
            reply_to_endpoint=self.find_msg_broker_endpoint(self.dt_entry_ins.identifier),
            service_type="fml40::AcceptsLogTruckWeightMeasurement/acceptFullWeightMeasurement",
            parameters={"receiptNumber": receipt_number},
        )
        APP_LOGGER.info("Send message to request log truck full weight measurement")
        self.add_broker_message_to_send(
            [self.find_msg_broker_endpoint(self.__cred_json["log_truck_scale"]["id"])],
            serv_req.base_msg
        )

    def send_log_truck_empty_measurement_job(self, receipt_number):
        serv_req = ServiceRequest()
        serv_req.fillServiceRequest(
            sender=self.dt_entry_ins.identifier,
            receivers=[self.__cred_json["log_truck_scale"]["id"]],
            message_id="s3i:{}".format(str(uuid.uuid4())),
            reply_to_endpoint=self.find_msg_broker_endpoint(self.dt_entry_ins.identifier),
            service_type="fml40::AcceptsLogTruckWeightMeasurement/acceptEmptyWeightMeasurement",
            parameters={"receiptNumber": receipt_number},
        )
        APP_LOGGER.info("Send message to request log truck empty measurement")
        self.add_broker_message_to_send(
            [self.find_msg_broker_endpoint(self.__cred_json["log_truck_scale"]["id"])],
            serv_req.base_msg
        )

    def send_wood_moisture_measurement_job(self, receipt_number):
        serv_req = ServiceRequest()
        serv_req.fillServiceRequest(
            sender=self.dt_entry_ins.identifier,
            receivers=[self.__cred_json["laboratory"]["id"]],
            message_id="s3i:{}".format(str(uuid.uuid4())),
            reply_to_endpoint=self.find_msg_broker_endpoint(self.dt_entry_ins.identifier),
            service_type="fml40::AcceptsMoistureMeasurement/acceptMoistureMeasurement",
            parameters={"receiptNumber": receipt_number},
        )
        APP_LOGGER.info("Send message to request wood moisture measurement")
        self.add_broker_message_to_send(
            [self.find_msg_broker_endpoint(self.__cred_json["laboratory"]["id"])],
            serv_req.base_msg
        )

    def send_wood_to_yard(self, receipt_number, log_loading_unit):
        serv_req = ServiceRequest()
        serv_req.fillServiceRequest(
            sender=self.dt_entry_ins.identifier,
            receivers=[self.__cred_json["wood_yard"]["id"]],
            message_id="s3i:{}".format(str(uuid.uuid4())),
            reply_to_endpoint=self.find_msg_broker_endpoint(self.dt_entry_ins.identifier),
            service_type="fml40::AcceptsLogLoadingUnitInWoodYard/accept",
            parameters={"receiptNumber": receipt_number, "logLoadingUnit": log_loading_unit},
        )
        APP_LOGGER.info("Send log loading unit to wood yard to be stored")
        self.add_broker_message_to_send(
            [self.find_msg_broker_endpoint(self.__cred_json["wood_yard"]["id"])],
            serv_req.base_msg
        )

    def send_delivery_confirmation_event(self, receipt_number):
        # TODO clear, where can we obtain orderID and referenceNumber, or ContractNumber
        atro = float(requests.get(self.__vato_url + "delivery_access/atro_weight/{}".format(receipt_number)).text)
        lutro = float(requests.get(self.__vato_url + "delivery_access/lutro_weight/{}".format(receipt_number)).text)
        contract_number = requests.get(self.__vato_url + "delivery_access/contract_nr/{}".format(receipt_number)).text
        transport_order_id = requests.get(self.__vato_url + "delivery_access/reference_nr/{}".format(receipt_number)).text
        topic = f"{self.dt_entry_ins.identifier}.transportationConfirmation"
        content = {
            "transportOrderId": transport_order_id,
            "contractNumber": contract_number,
            "atro": atro,
            "lutro": lutro
        }
        self.add_broker_event_message_to_send(
            topic=topic,
            content=content
        )
        APP_LOGGER.info("Send delivery confirmation event")

    def on_service_reply(self, msg):
        if msg.get("serviceType") == "fml40::AcceptsMoistureMeasurement/acceptMoistureMeasurement":
            APP_LOGGER.info("Moisture measurement arrives: {}".format(msg.get("results")))
            requests.post(
                url=self.__vato_url + "delivery_access/moisture_level/{}".format(msg.get("results")["receiptNumber"]),
                json={
                    "value": msg.get("results")["humidity"],
                    "timeStamp": int(time.time())
                }
            )
            self.send_log_truck_full_measurement_job(msg.get("results")["receiptNumber"])

        if msg.get("serviceType") == "fml40::AcceptsLogTruckWeightMeasurement/acceptFullWeightMeasurement":
            APP_LOGGER.info("Log truck weight measurement arrives: {}".format(msg.get("results")))
            requests.post(
                url=self.__vato_url + "delivery_access/full_weight/{}".format(msg.get("results")["receiptNumber"]),
                json={
                    "value": msg.get("results")["weight"],
                    "timeStamp": int(time.time())
                }
            )
            log_loading_unit_resp = requests.get(
                url=self.__vato_url + "delivery_access/log_loading_unit/{}".format(msg.get("results")["receiptNumber"])
            )
            self.send_unload_request(
                log_loading_unit_resp.json().get("thingId"),
                receipt_number=msg.get("results")["receiptNumber"])

        elif msg.get("serviceType") == "fml40::UnloadsLogLoadingUnit/unload":
            if msg.get("results")["ok"]:
                all_deliveries = requests.get(
                    url=self.__vato_url + "delivery/all"
                )
                all_deliveries = ast.literal_eval(all_deliveries.text)
                for delivery in all_deliveries:
                    llu_json = json.loads(delivery[-1])
                    if llu_json["thingId"] == msg.get("results")["identifier"]:
                        APP_LOGGER.info(
                            "Log truck has unloaded the log loading unit {} corresponding to the receipt number {}".format(
                                msg.get("results")["identifier"], delivery[0]))
                        requests.post(
                            url=self.__vato_url + "delivery_access/unloaded/{}".format(delivery[0]),
                            json={"value": msg.get("results")["ok"]}
                        )

                        self.send_log_truck_empty_measurement_job(
                            delivery[0]
                        )

            else:
                APP_LOGGER.info(
                    "Log Truck can NOT unload the log loading unit {}".format(msg.get("results")["identifier"]))

        if msg.get("serviceType") == "fml40::AcceptsLogTruckWeightMeasurement/acceptEmptyWeightMeasurement":
            APP_LOGGER.info("Log truck weight measurement arrives: {}".format(msg.get("results")))
            receipt_number = msg.get("results")["receiptNumber"]
            empty_weight = msg.get("results")["weight"]
            requests.post(
                url=self.__vato_url + "delivery_access/empty_weight/{}".format(receipt_number),
                json={
                    "value": empty_weight,
                    "timeStamp": int(time.time())
                }
            )
            log_loading_unit_resp = requests.get(
                url=self.__vato_url + "delivery_access/log_loading_unit/{}".format(receipt_number)
            )
            full_weight_response = requests.get(
                url=self.__vato_url + "delivery_access/full_weight/{}".format(receipt_number)
            )
            moisture_level_response = requests.get(
                url=self.__vato_url + "delivery_access/moisture_level/{}".format(receipt_number)
            )
            lutro = float(full_weight_response.json()["value"]) - float(empty_weight)
            requests.post(
                url=self.__vato_url + "delivery_access/lutro_weight/{}".format(receipt_number),
                json={
                    "value": lutro
                }
            )
            atro = (1 - float(moisture_level_response.json()["value"])) * lutro
            APP_LOGGER.info("Delivery with receiptnumber {}: Lutro: {}, atro: {}".format(receipt_number, lutro, atro))
            requests.post(
                url=self.__vato_url + "delivery_access/atro_weight/{}".format(receipt_number),
                json={
                    "value": atro
                }
            )
            # close the delivery
            close_delivery_resp = requests.delete(
                url=self.__vato_url + "delivery/{}".format(receipt_number)
            )
            if close_delivery_resp.status_code == 204:
                APP_LOGGER.info("Delivery with receipt number {} closed".format(receipt_number))
                self.send_wood_to_yard(msg.get("results")["receiptNumber"], log_loading_unit_resp.json())

        if msg.get("serviceType") == "fml40::AcceptsLogLoadingUnitInWoodYard/accept":
            APP_LOGGER.info("Log loading unit from receipt {} moved to wood yard and stored in storage box {}".format(
                msg.get("results")["receiptNumber"], msg.get("results")["storageBoxIds"]
            ))
            requests.post(
                url=self.__vato_url + "delivery_access/storage_box_id/{}".format(msg.get("results")["receiptNumber"]),
                json={
                    "value": json.dumps(msg.get("results")["storageBoxIds"])
                }
            )
            self.send_delivery_confirmation_event(msg.get("results")["receiptNumber"])


class MillGate(Thing):
    def __init__(self):
        with open("config/cred.json", "r") as cred_str:
            cred_json = json.load(cred_str)
            oauth2_id = cred_json["mill_gate"]["id"]
            oauth2_secret = cred_json["mill_gate"]["secret"]
        mill_gate["thingId"] = oauth2_id
        mill_gate["policyId"] = oauth2_id
        mill_gate["attributes"]["name"] = "UPM Leuna Biometrics MillGate"
        mill_gate["attributes"]["features"].append(
            {
                "class": "ml40::EventList",
                "subFeatures": [
                    {
                        "class": "ml40::Event",
                        "topic": f"{oauth2_id}.transportationConfirmation",
                        "description": "Event triggered if a transportation order is finished",
                        "frequency": 1,
                        "schema": {},
                        "exampleContent": {
                            "transportOrderId": "",
                            "contractNumber": "",
                            "atro": 0.1,
                            "lutro": 0.1
                        }
                    }
                ]
            }
        )
        setup_logger("MillGate")
        entry = build(mill_gate)
        loop = asyncio.get_event_loop()
        connector = MyS3IConnector(
            entry=entry,
            loop=loop,
            oauth2_id=oauth2_id,
            oauth2_secret=oauth2_secret,
            vato_url=cred_json["mill_gate"]["host"]
        )
        super(MillGate, self).__init__(
            entry=entry,
            loop=loop,
            connector=connector
        )
        self.__vato_url = cred_json["mill_gate"]["host"]
        self.receipt_number = None
        self.log_loading_unit_entry = None
        self.__update_model()

    @property
    def vato_url(self):
        return self.__vato_url

    def __update_model(self):
        self.entry.name = "UPM Millgate"
        self.entry.features["ml40::OrganizationalContact"].name = "Biochemicals Leuna"
        self.entry.features["ml40::OrganizationalContact"].street = "Tor 6 - Bau 4400"
        self.entry.features["ml40::OrganizationalContact"].city = "Leuna"
        self.entry.features["ml40::OrganizationalContact"].zip = "06237"

    def calculate_lutro(self, fullWeight, emptyWeight):
        return fullWeight - emptyWeight

    def calculate_atro(self, lutro, moistureLevel):
        return moistureLevel * lutro

    def run(self):
        self.add_ml40_implementation(AcceptsLogLoadingUnitImpl, "fml40::AcceptsLogLoadingUnit", self)
        self.run_forever()
