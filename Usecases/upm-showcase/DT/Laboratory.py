import json
from dt_templates.json_models import laboratory
from ml import Thing, build, S3IConnector, S3IParameter, setup_logger, APP_LOGGER
from ml.fml40.features.functionalities.accepts_moisture_measurement import AcceptsMoistureMeasurement
import aioconsole
import asyncio


class AcceptsMoistureMeasurementImpl(AcceptsMoistureMeasurement):
    def __init__(self, labor, name="", identifier=""):
        super(AcceptsMoistureMeasurementImpl, self).__init__(
            name=name,
            identifier=identifier
        )
        self.labor = labor

    async def acceptMoistureMeasurement(self, receiptNumber):
        while True:
            moisture_value = await aioconsole.ainput("Please give the measured moisture value [0-1]: ")
            try:
                moisture_value_float = float(moisture_value)
                if 0 <= moisture_value_float <= 1:
                    APP_LOGGER.info("Send the moisture value {} back to mill gate".format(moisture_value))
                    return {"humidity": float(moisture_value), "receiptNumber": receiptNumber}
                else:
                    APP_LOGGER.critical("The moisture value is supposed to be set between 0 and 1")
                    continue
            except ValueError:
                APP_LOGGER.critical("Please enter a value between 0 and 1")
                continue

class Laboratory(Thing):
    def __init__(self):
        with open("config/cred.json", "r") as cred_str:
            cred_json = json.load(cred_str)
            oauth2_id = cred_json["laboratory"]["id"]
            oauth2_secret = cred_json["laboratory"]["secret"]
        laboratory["thingId"] = oauth2_id
        laboratory["policyId"] = oauth2_id
        laboratory["attributes"]["name"] = "UPM Laboratory"
        loop = asyncio.get_event_loop()
        entry = build(laboratory)
        connector = S3IConnector(
            oauth2_id=oauth2_id,
            oauth2_secret=oauth2_secret,
            is_repository=False,
            is_broker_rest=False,
            is_broker=True,
            dt_entry_ins=entry,
            loop=loop
        )
        setup_logger("UPM Laboratory")
        super(Laboratory, self).__init__(
            loop=loop,
            entry=entry,
            connector=connector
        )

    def run(self):
        self.add_ml40_implementation(AcceptsMoistureMeasurementImpl, "fml40::AcceptsMoistureMeasurement", self)
        self.run_forever()
