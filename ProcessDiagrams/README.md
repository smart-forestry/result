# Process diagrams as PDFs


All PlantUML diagrams are also provided as PDFs here:
 |**List of 5 identified sequences**|
 |:--------------------------------|
 | highly mechanized timber harvesting [puml](https://git.rwth-aachen.de/smart-forestry/result/-/blob/main/ProcessDiagrams/highly_mech_timber_harvesting.puml?ref_type=heads)/[pdf](https://git.rwth-aachen.de/smart-forestry/result/-/jobs/artifacts/main/file/public/ProcessDiagrams/pdfs/SequenceDiagram_HSM-UseCase.pdf?job=buildPDF)            |
|logistics [puml](https://git.rwth-aachen.de/smart-forestry/result/-/blob/main/ProcessDiagrams/logistics.puml?ref_type=heads)/[pdf](https://git.rwth-aachen.de/smart-forestry/result/-/jobs/artifacts/main/file/public/ProcessDiagrams/pdfs/SequenceDiagramLogistics.pdf?job=buildPDF)         |
|manual timber harvesting [puml](https://git.rwth-aachen.de/smart-forestry/result/-/blob/main/ProcessDiagrams/manual_timber_harvesting.puml?ref_type=heads)/[pdf](https://git.rwth-aachen.de/smart-forestry/result/-/jobs/artifacts/main/file/public/ProcessDiagrams/pdfs/SequenceDiagram_STIHL-UseCase.pdf?job=buildPDF)          |
|planning and commissioning [puml](https://git.rwth-aachen.de/smart-forestry/result/-/blob/main/ProcessDiagrams/planning_and_commissioning.puml?ref_type=heads)/[pdf](https://git.rwth-aachen.de/smart-forestry/result/-/jobs/artifacts/main/file/public/ProcessDiagrams/pdfs/planning%20and%20commissioning.pdf?job=buildPDF)         |
|wood accept[puml](https://git.rwth-aachen.de/smart-forestry/result/-/blob/main/ProcessDiagrams/wood_accept.puml?ref_type=heads)/[pdf](https://git.rwth-aachen.de/smart-forestry/result/-/jobs/artifacts/main/file/public/ProcessDiagrams/pdfs/wood_accept.pdf?job=buildPDF)            |
