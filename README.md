## Result
[Smart Forestry](https://www.kwh40.de/smartforestry/) is a joint project of the partners RWTH Aachen University (consortium leader), Hohenloher Spezial-Maschinenbau GmbH & Co. KG (HSM), Bayerische Staatsforsten AöR (BaySF), iFOS GmbH, ANDREAS STIHL AG & Co. KG, UPM Biochemicals GmbH, Kuratorium für Waldarbeit und Forsttechnik (KWF) e.V. and Landesbetrieb Wald und Holz NRW (Forstliches Bildungszentrum).

<center><img src="logo/logo.png" alt="drawing" width="300"/></center>

The aim of the Smart Forestry joint project is to develop cross-cluster approaches based on Forestry 4.0 concepts for intelligent, fully integrated timber harvesting. Based on digital twins, the Smart Forestry concept networks all stakeholders along the wood supply chain for the first time - from felling planning to delivery to the customer. Using situation-specific, freely configurable value creation networks, it enables all stakeholders to communicate "at eye level", control, evaluate and optimize the timber harvesting process and integrate it into upstream and downstream process stages.

The project is funded by the Federal Ministry of Food and Agriculture (BMEL) via its project management agency Fachagentur Nachwachsende Rohstoffe (FNR) e.V. (funding code 2220NR254 A-H). The duration of this 3-year project is from 10/2021 to 09/2024.

This GitLab repository operates as a storage for the project's (intermediary) results. 

## Overview 

This repository includes various use cases that make use of the [ForestML 4.0 (fml40) reference implementation](https://git.rwth-aachen.de/kwh40/fml40-reference-implementation). The following use cases are showcased:

- **BaySF**: Planning, commissioning and control (_to be published_)
- **HSM**: Highly mechanized timber harvesting (_to be published_)
- **STIHL**: Motorized manual timber harvesting (_to be published_) 
- **UPM**: Wood delivery to a mill, passing through the mill's gate.


This repository includes:

- **DigitalTwinTemplates**: Templates for the digital twins involved in each use case (UML diagrams).
- **MessageTemplates**: JSON schemas of the S3I-B messages exchanged between the digital twins.
- **ProcessDiagrams**: Sequence diagrams depicting the process of each use case. 
- **Usecases**: Python scripts that create and run the digital twins.

The UML diagrams are created using PlantUML (`.puml` or `.plantuml`). You can view these files using GitLab's inbuilt viewer or the provided PDF files.

## Built With

- [ForestML 4.0 (fml40) reference implementation](https://git.rwth-aachen.de/kwh40/fml40-reference-implementation): 
Used to create the digital twins, also see [here](https://ieeexplore.ieee.org/document/10121858).
- [S3I](https://git.rwth-aachen.de/kwh40/s3i): 
Offers the minimum set of central services used by the fml40-reference-implementation to provide IoT-based interconnections. See [S3I whitepaper](https://www.kwh40.de/wp-content/uploads/2022/02/KWH40-Standpunkt-S3I-EN.pdf) for more details. 

