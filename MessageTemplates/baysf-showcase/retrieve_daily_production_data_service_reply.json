{
   "$schema":"http://json-schema.org/draft-07/schema#",
   "type":"object",
   "description":"a s3i-b service reply containing information about production data",
   "properties":{
      "sender":{
         "type":"string",
         "description":"s3i id of production team"
      },
      "receivers":{
         "type":"array",
         "item":{
            "type":"string",
            "description":"s3i id of forest owner",
            "const":"s3i:d698ae55-7a5f-4cc2-94b0-940c7479df23"
         }
      },
      "identifier":{
         "type":"string",
         "description":"id of the message"
      },
      "messageType":{
         "const":"serviceReply"
      },
      "serviceType":{
         "const":"fml40::ProvidesProductionData/provide"
      },
      "replyingToMessage":{
         "type":"string",
         "description":"identifier of the previous service request"
      },
      "results":{
         "type":"array",
         "item": {
            "type": "object",
            "required":[
            "orderDetailId",
            "date",
            "harvesting",
            "forwarding",
            "transportation"
         ],
         "properties":{
            "orderDetailId":{
               "type":"string"
            },
            "date":{
               "type":"string",
				"description":"date in YYYY-MM-DD format, e.g., 2024-05-24",
				"pattern": "[0-9]{4}-[0-9]{2}-[0-9]{2}"
            },
            "harvesting":{
               "type":"object",
               "required":[
                  "totalAmount"
               ],
               "properties":{
                  "totalAmount":{
                     "type":"number",
                     "description":"total harvested amount in FM for this orderDetailId"
                  },
                  "treeCoordinates":{
                     "type":"embedded geoJSON",
                     "description":"coordinates of harvested trees in geoJSON format, points for this orderDetailId"
                  },
                  "harvestingStatus":{
                     "type":"string",
                     "enum":[
                        "PENDING",
                        "STARTED",
                        "RUNNING",
                        "FINISHED"
                     ]
                  }
               }
            },
            "forwarding":{
               "type":"object",
               "required":[
                  "woodPiles"
               ],
               "properties":{
                  "woodPiles":{
                     "type":"array",
                     "items":{
                        "type":"object",
                        "required":[
                           "s3iId",
                           "totalAmountForwarded",
                           "totalAmountTransported",
                           "coordinates"
                        ],
                        "properties":{
                           "s3iId":{
                              "type":"string",
                              "description":"s3iId of the wood pile"
                           },
                           "totalAmountForwarded":{
                              "type":"number",
                              "description":"total forwarded amount in FM to this woodPile"
                           },
                           "totalAmountTransported":{
                              "type":"number",
                              "description":"total transported amount in FM from this woodPile"
                           },
                           "woodPileType":{
                              "type":"string",
                              "enum":[
                                 "STATIC",
                                 "FLOAT"
                              ]
                           },
                           "forwardingStatus":{
                              "type":"string",
                              "enum":[
                                 "PENDING",
                                 "STARTED",
                                 "RUNNING",
                                 "FINISHED"
                              ]
                           },
                           "transportationStatus":{
                              "type":"string",
                              "enum":[
                                 "PENDING",
                                 "STARTED",
                                 "RUNNING",
                                 "FINISHED"
                              ]
                           },
                           "coordinates":{
                              "type":"object",
                              "properties":{
                                 "longitude":{
                                    "type":"number",
                                    "description":"longitude of the woodpile in WGS84 EPSG 4326"
                                 },
                                 "latitude":{
                                    "type":"number",
                                    "description":"latitude of the woodpile in WGS84 EPSG 4326"
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            },
            "transportation":{
               "type":"object",
               "required":[
                  "totalAmountInTransportation",
                  "totalAmountDelivered",
                  "deliveryNotes"
               ],
               "properties":{
                  "totalAmountInTransportation":{
                     "type":"number",
                     "description":"total amount of transportation in FM for this orderDetailId"
                  },
                  "totalAmountDelivered":{
                     "type":"number",
                     "description":"total amount of delivered transportation (approved by customer) in FM for this orderDetailId"
                  },
                  "deliveryNotes":{
                     "type":"array",
                     "items":{
                        "type":"object",
                        "desctipion":"EldatSmart based JSON for delivery note"
                     },
                     "description":" FAP_prodDataDeliveryNotes, unique key: PROD_DATA_DAILY_ID, s3iId"
                  }
               }
            }
         }
         }
      }
   }
}